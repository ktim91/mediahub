<!DOCTYPE html>
<html>
<head>
	<title>Mediahub - front</title>
	<link rel="stylesheet" type="text/css" href="./styles/main.css">
</head>
<body>
	<header>
		<div class="head flex flex-jc-sb flex-vcenter">
			<a href="#" class="logo">
				<img src="./images/logo.svg">
			</a>
			<ul class="menu flex flex-vcenter">
				<li><a href="#">О проекте</a></li>
				<li><a href="#">Тарифные планы</a></li>
			</ul>
			<div class="auth_lang flex flex-vcenter">
				<div class="lang auth_lang-item">
					<button class="btn lang-btn">
						Ру
						<img src="./images/icons/arrow-down.svg">
					</button>
					<!-- <div class="lang-list"></div> -->
				</div>
				<a href="#" class="btn btn-dark auth_lang-item">Вход</a>
			</div>
		</div>
	</header>