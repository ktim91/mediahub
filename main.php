<?php 
require_once('header.php'); 

$screens = array(
	array(
		'title' => 'Медиахаб',
		'sub_title' => 'фотографии с центральной азии',
		'img' => './images/img-1.png',
		'class' => 'first-section'
	),
	array(
		'title' => 'Люди',
		'sub_title' => 'В Медиахабе можно найти лица людей из Казахстана, Киргизии, Узбекистана, Таджикистана и Туркмении',
		'img' => './images/img-2.png',
		'class' => 'second-section'
	),
	array(
		'title' => 'Места',
		'sub_title' => 'У нас больше чем 5498 фотографий культурных мест и достопримечательностей средней азии',
		'img' => './images/img-3.png',
		'class' => 'third-section'
	),
	array(
		'title' => 'События',
		'sub_title' => 'События и культурные праздники центральноазиатских стран от Наурыза до Курбан-Айта и выборов президента',
		'img' => './images/img-3.png',
		'class' => 'four-section'
	),
	array(
		'nad_title' => 'Для каких ситуаций подходит',
		'title' => 'Пост для соцсетей',
		'sub_title' => 'Если у вас пост связанный с локальным праздником и вам нужны местные люди и фотографии с праздника',
		'img' => './images/img-5.png',
		'class' => 'five-section'
	),
	array(
		'nad_title' => 'Для каких ситуаций подходит',
		'title' => 'Графический дизайн',
		'sub_title' => 'Для наружной рекламы и дизайн-макетов, чтобы отличиться от конкурентов, и не ставить одну и ту же фотографию с шаттерстока',
		'img' => './images/img-6.png',
		'class' => 'six-section'
	),
	array(
		// 'nad_title' => 'Для каких ситуаций подходит',
		'title' => 'Сравнение',
		'sub_title' => 'Представьте, что вам нужно сделать качественную наружную рекламу по теме праздника Наурыз, и для этого нужно найти хорошие фотографии людей в праздничной атрибутике на фоне казахского аула',
		// 'img' => './images/img-6.png',
		'class' => 'seven-section',
		'tabl' => array(
			'row1' => array(
				'cell1' => 'Центральноазиатские фотографии',
				'cell2' => './images/icons/nope.svg',
				'cell3' => './images/icons/yeah.svg',
				'cell4' => './images/icons/yeah-opacity.svg'
			),
			'row2' => array(
				'cell1' => 'Большой ассортимент',
				'cell2' => './images/icons/yeah-opacity.svg',
				'cell3' => './images/icons/yeah.svg',
				'cell4' => './images/icons/nope.svg'
			),
			'row3' => array(
				'cell1' => 'Небольшие временные затраты',
				'cell2' => './images/icons/yeah-opacity.svg',
				'cell3' => './images/icons/yeah.svg',
				'cell4' => './images/icons/nope.svg'
			),
			'row4' => array(
				'cell1' => 'Доступная цена',
				'cell2' => './images/icons/nope.svg',
				'cell3' => './images/icons/yeah.svg',
				'cell4' => './images/icons/nope.svg'
			),
		)
	),
	array(
		'title' => 'Получите ранний доступ к Медиахабу',
		'class' => 'eighth-section'
	),
);
?>

<main>

	<?php foreach($screens as $screen) : ?>
		<section class="section <?php echo $screen['class']; ?> promo_section flex">
			<div class="container">

				<h2 class="sub_title nad_title"><?php echo $screen['nad_title']; ?></h2>
				<h1 class="title"><?php echo $screen['title']; ?></h1>
				<h2 class="sub_title"><?php echo $screen['sub_title']; ?></h2>
				<img src="<? echo $screen['img']; ?>">

				<?php if(isset($screen['tabl'])) : ?>
					<div class="tabl">
						<div class="tabl-row">
							<div class="tabl-cell">
								Способ поиска
							</div>
							<div class="tabl-cell">
								Привычные фотостоки
							</div>
							<div class="tabl-cell">
								Медиахаб
							</div>
							<div class="tabl-cell">
								Заказ фотосъемки
							</div>
						</div>
						<?php foreach($screen['tabl'] as $row) : ?>
							<div class="tabl-row">
								<div class="tabl-cell">
									<? echo $row['cell1']; ?>
								</div>
								<div class="tabl-cell">
									<img src="<? echo $row['cell2']; ?>">
								</div>
								<div class="tabl-cell">
									<img src="<? echo $row['cell3']; ?>">
								</div>
								<div class="tabl-cell">
									<img src="<? echo $row['cell4']; ?>">
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

			</div>
		</section>
	<?php endforeach; ?>

</main>

<?php require_once('footer.php'); ?>